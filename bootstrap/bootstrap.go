package bootstrap

import (
	"app/pkg/console"
	"app/pkg/model"
	"app/pkg/route"
)

type bootstrap struct{}

func Run() {
	var b bootstrap
	b.startDB()
	b.startRdb()
	b.startCmd()
	b.startRoute()
}

// 启动数据库
func (b *bootstrap) startDB() {
	model.NewMysql()
}

// 启动Reids
func (b *bootstrap) startRdb() {
	model.NewRedis()
}

// 启动路由
func (b *bootstrap) startRoute() {
	route.NewRoute()
}

// 启动定时任务
func (b *bootstrap) startCmd() {
	console.NewCmd()
}
