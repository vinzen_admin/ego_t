package console

import "app/console/commands"

// 注册定时任务
var (
	console commands.Console
)

func Terminate() {
	console.Handler()
}
