package example

import (
	"app/pkg/ejwt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type AuthController struct{}

type UserInfo struct {
	Username string `form:"username" json:"username"`
	Password string `form:"password" json:"password"`
}

func (*AuthController) Login(c *gin.Context) {
	// 用户发送用户名和密码过来
	var user UserInfo
	err := c.ShouldBind(&user)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": 1,
			"msg":  "无效的参数",
		})
		return
	}
	// 校验用户名和密码是否正确
	if user.Username == "admin" && user.Password == "123456" {
		// 生成Token
		tokenString, _ := ejwt.GenToken(1, "admin")
		c.JSON(http.StatusOK, gin.H{
			"code": 0,
			"msg":  "success",
			"data": gin.H{"token": tokenString},
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 1,
		"msg":  "鉴权失败",
	})
}

// 验证用户
func (*AuthController) VerAuth(c *gin.Context) {
	user_id := c.GetUint("user_id")
	username := c.GetString("username")
	c.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "success",
		"data": gin.H{
			"user_id":  user_id,
			"username": username,
		},
	})
}
