package example

import "github.com/gin-gonic/gin"

type HomeController struct {
}

func (*HomeController) Ego(c *gin.Context) {
	c.String(200, "Hello World")
}

func (*HomeController) Index(c *gin.Context) {
	c.HTML(200, "welcome.html", nil)
}

func (*HomeController) Image(c *gin.Context) {
	c.File("static/file/1.png")
}
