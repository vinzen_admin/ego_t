package example

import (
	"app/pkg/model"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

type RedisController struct {
}

func (*RedisController) SGName(c *gin.Context) {
	var (
		err error
		val string
	)
	err = model.RDB.Set("name", "xxxdddd", 0).Err()
	if err != nil {
		fmt.Println("redis 设置出错", err)
		return
	}
	fmt.Println("redis 设置成功")
	fmt.Println("正在取出redis值...")
	time.Sleep(time.Microsecond * 2)

	val, err = model.RDB.Get("name").Result()
	if err == redis.Nil {
		fmt.Println("没有检测到这个值", err)
		return
	}
	if err != nil {
		fmt.Println("取值失败", err)
		return
	}
	fmt.Println("取值成功", val)
	c.JSON(200, gin.H{
		"code": 0,
		"data": val,
	})
}
