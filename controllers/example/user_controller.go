package example

import (
	"app/models"
	"app/pkg/model"

	"github.com/gin-gonic/gin"
)

type UserController struct {
}

func (*UserController) CreateUser(c *gin.Context) {
	var user = models.Users{
		UserName: "张三",
		Age:      10,
	}
	err := user.Add()
	if err != nil {
		c.JSON(200, gin.H{
			"code":   1,
			"msg":    "插入失败",
			"reason": err,
		})
		return
	}
	c.JSON(200, gin.H{
		"code": 0,
		"msg":  "插入成功",
	})
}

func (*UserController) Delete(c *gin.Context) {
	id := c.PostForm("id")
	var u models.Users
	err := u.Del(id)
	if err != nil {
		c.JSON(200, gin.H{
			"code":   1,
			"msg":    "删除失败",
			"reason": err,
		})
		return
	}
	c.JSON(200, gin.H{
		"code": 0,
		"msg":  "删除成功",
	})
}

func (*UserController) Edit(c *gin.Context) {
	id := c.PostForm("id")
	var u models.Users
	err := u.Edit(id)
	if err != nil {
		c.JSON(200, gin.H{
			"code":   1,
			"msg":    "修改失败",
			"reason": err,
		})
		return
	}
	c.JSON(200, gin.H{
		"code": 0,
		"msg":  "修改成功",
	})
}

func (*UserController) FindUserByID(c *gin.Context) {
	id := c.Query("id")
	var (
		u   models.Users
		err error
	)
	u, err = u.GetUserByID(id)
	if err != nil {
		c.JSON(200, gin.H{
			"code":   1,
			"msg":    "获取失败",
			"reason": err,
		})
		return
	}
	c.JSON(200, gin.H{
		"code": 0,
		"msg":  "获取成功",
		"data": u,
	})
}

func (*UserController) FindUserAll(c *gin.Context) {
	var Users []models.Users
	username := c.Query("user_name")
	age := c.Query("age")
	tx := model.DB.Table("users")

	if len(username) > 0 {
		tx.Where("user_name", username)
	}

	if len(age) > 0 {
		tx.Where("age", age)
	}

	if err := tx.Find(&Users).Error; err != nil {
		c.JSON(200, gin.H{
			"code":   1,
			"msg":    "获取失败",
			"reason": err,
		})
		return
	}

	c.JSON(200, gin.H{
		"code": 0,
		"msg":  "获取成功",
		"data": Users,
	})

}
