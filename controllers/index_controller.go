package controllers

import "github.com/gin-gonic/gin"

type IndexController struct{}

// @BasePath /api/v1

// @Tags 首页
// @Summary 首页
// @Accept json
// @Produce json
// @Success 200 {string} Hello World
// @Router /index [get]
func (*IndexController) Index(c *gin.Context) {
	c.String(200, "Hello World")
}

// @Tags 首页
// @Summary 详情
// @Accept json
// @Produce json
// @Success 200 {string} Hello Show
// @Router /show [get]
func (*IndexController) Show(c *gin.Context) {
	c.String(200, "Hello Show")
}
