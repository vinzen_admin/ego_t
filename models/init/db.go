package main

import (
	"app/models"
	"app/pkg/model"
	"fmt"
)

func main() {
	err := model.NewMysql().Set("gorm:table_options", "ENGINE=InnoDB").
		AutoMigrate(
			&models.Users{})
	if err != nil {
		fmt.Println("创建表失败", err)
		return
	}
	fmt.Println("初始化User表成功")
}
