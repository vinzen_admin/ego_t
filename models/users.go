package models

import (
	"app/pkg/model"
	"time"
)

type Users struct {
	ID        int        `json:"id" gorm:"AUTO_INCREMENT"`
	UserName  string     `json:"username" gorm:"type:varchar(100);not null"`
	Age       int        `json:"age" gorm:"type:int(2);not null"`
	CreatedAt *time.Time `json:"created_at" gorm:"type:datetime(0);"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"type:datetime(0);"`
}

func (u *Users) Add() error {
	return model.DB.Create(&u).Error
}

func (u *Users) Del(id string) error {
	return model.DB.Delete(&u, id).Error
}

func (u *Users) Edit(id string) error {
	_, err := u.GetUserByID(id)
	if err != nil {
		return err
	}
	return model.DB.Where("id", id).Updates(Users{
		UserName: "哈哈哈",
		Age:      18,
	}).Error
}

func (u *Users) GetUserByID(id string) (user Users, err error) {
	var userData Users
	if err := model.DB.First(&userData, id).Error; err != nil {
		return userData, err
	}
	return userData, nil
}

func (u *Users) GetUserByAll() (user []Users, err error) {
	var userData []Users
	if err := model.DB.Find(&userData).Error; err != nil {
		return userData, err
	}
	return userData, nil
}
