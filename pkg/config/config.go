package config

import (
	"log"

	"github.com/spf13/viper"
)

// Viper Viper 库实例
var Viper *viper.Viper

// init() 函数在 import 的时候立刻被加载
func init() {
	// 1. 初始化 Viper 库
	Viper = viper.New()
	// 2. 设置文件名称
	Viper.SetConfigName("config")
	// 3. 配置类型，支持 "json", "toml", "yaml", "yml", "properties",
	//             "props", "prop", "env", "dotenv"
	Viper.SetConfigType("yaml")
	// 4. 环境变量配置文件查找的路径，相对于 main.go

	Viper.AddConfigPath(".")
	// 5. 开始读根目录下的
	//尝试进行配置读取
	if err := Viper.ReadInConfig(); err != nil {
		log.Fatal("读取配置文件失败", err.Error())
	}
}
