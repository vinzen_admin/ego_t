package model

import (
	"app/pkg/config"
	"log"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func NewMysql() *gorm.DB {
	if DB == nil {
		setupDB()
	}
	return DB
}

func setupDB() {
	var err error

	db_host := config.Viper.GetString("database.db_host")
	db_port := config.Viper.GetString("database.db_port")
	db_database := config.Viper.GetString("database.db_database")
	db_username := config.Viper.GetString("database.db_username")
	db_password := config.Viper.GetString("database.db_password")

	if len(db_host) == 0 ||
		len(db_port) == 0 ||
		len(db_username) == 0 ||
		len(db_database) == 0 {
		return
	}

	config := mysql.New(mysql.Config{
		DSN: db_username + ":" +
			db_password +
			"@tcp(" + db_host + ":" + db_port + ")/" + db_database +
			"?charset=utf8&parseTime=True&loc=Local",
	})

	// 准备数据库连接池
	DB, err = gorm.Open(config, &gorm.Config{})

	if err != nil {
		log.Fatal("数据库打开失败", err)
	}

	sqlDB, _ := DB.DB()

	// 设置最大连接数
	sqlDB.SetMaxOpenConns(100)
	// 设置最大空闲连接数
	sqlDB.SetMaxIdleConns(25)
	// 设置每个链接的过期时间
	sqlDB.SetConnMaxLifetime(5 * time.Minute)
}
