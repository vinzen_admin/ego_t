package model

import (
	"app/pkg/config"
	"log"

	"github.com/go-redis/redis"
)

var RDB *redis.Client

func NewRedis() *redis.Client {
	if RDB == nil {
		setupRedis()
	}
	return RDB
}

func setupRedis() {
	rdb_host := config.Viper.GetString("redis.rdb_host")
	rdb_port := config.Viper.GetString("redis.rdb_port")
	rdb_password := config.Viper.GetString("redis.rdb_password")

	if len(rdb_host) == 0 || len(rdb_port) == 0 {
		return
	}

	RDB = redis.NewClient(&redis.Options{
		Addr:     rdb_host + ":" + rdb_port,
		Password: rdb_password, // no password set
		DB:       0,            // use default DB
	})

	_, err := RDB.Ping().Result()
	if err != nil {
		log.Fatal("redis启动失败", err)
	}
}
