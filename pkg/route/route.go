package route

import (
	"app/docs"
	"app/pkg/config"
	"app/routes"
	"log"

	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func NewRoute() {

	route := gin.Default()

	host := config.Viper.GetString("server.app_host")
	port := config.Viper.GetString("server.app_port")
	swag_server := config.Viper.GetBool("swag.swag_server")

	if swag_server {
		swagRun(route)
	}

	routes.RegisterApiRoutes(route)
	log.Fatal(route.Run(host + ":" + port))
}

// github:https://github.com/swaggo/gin-swagger
func swagRun(r *gin.Engine) {
	docs.SwaggerInfo.BasePath = "/api/v1"
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
}
