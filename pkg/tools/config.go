package tools

import (
	"bufio"
	"encoding/json"
	"os"
)

type Config struct {
	AppName      string `json:"app_name"`
	AppMode      string `json:"app_mode"`
	AppHost      string `json:"app_host"`
	AppPort      string `json:"app_port"`
	DBConnection string `json:"db_connection"`
	DBHost       string `json:"db_host"`
	DBPort       string `json:"db_port"`
	DBDatabase   string `json:"db_database"`
	DBUsername   string `json:"db_username"`
	DBPassword   string `json:"db_password"`
	RDBHost      string `json:"rdb_host"`
	RDBPort      string `json:"rdb_port"`
	RDBPassword  string `json:"rdb_password"`
}

var _cfg *Config = nil

func ParseConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	decoder := json.NewDecoder(reader)
	if err = decoder.Decode(&_cfg); err != nil {
		return nil, err
	}
	return _cfg, nil
}
