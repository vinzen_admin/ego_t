package tools

import (
	"crypto/md5"
	"fmt"
)

func Md5Sum(str string) string {
	md5str := fmt.Sprintf("%x", md5.Sum([]byte(str)))
	return md5str
}
