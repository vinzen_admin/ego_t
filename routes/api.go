package routes

import (
	"app/controllers"
	"app/controllers/example"

	"github.com/gin-gonic/gin"
)

func RegisterApiRoutes(route *gin.Engine) {

	v1 := route.Group("/api/v1")
	{
		// jwt 中间件
		// v1.Use(middleware.JWTAuthMiddleware())
		a := (new(example.AuthController))
		v1.POST("/auth", a.Login)
		v1.GET("/ver_auth", a.VerAuth)
		i := (new(controllers.IndexController))

		v1.GET("/show", i.Show)
		v1.GET("/index", i.Index)

	}
}
